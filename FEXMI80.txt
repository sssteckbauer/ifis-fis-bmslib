FEXMI80  DFHMSD TYPE=MAP,LANG=COBOL,TERM=3270,MODE=INOUT,              X
               TIOAPFX=YES,MAPATTS=(COLOR,HILIGHT),                    X
               DSATTS=(COLOR,HILIGHT)
FEXMI80  DFHMDI SIZE=(24,80),LINE=1,COLUMN=1,CTRL=FREEKB,TIOAPFX=YES
         DFHMDF POS=(01,01),LENGTH=0007,ATTRB=(PROT,ASKIP),            X
               INITIAL='FEXMI80',COLOR=BLUE,HILIGHT=OFF
* 'TRMNL-ID-SY00'
ZFLD002  DFHMDF POS=(01,10),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(01,19),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'FULL-NAME-6001-SY00'
ZFLD003  DFHMDF POS=(01,23),LENGTH=0035,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(01,59),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'AGR-DATE'
ZFLD004  DFHMDF POS=(01,64),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'WORK-HH-MM-TIME-SY00'
ZFLD005  DFHMDF POS=(01,73),LENGTH=0005,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'AM-PM-FLAG-SY00'
ZFLD006  DFHMDF POS=(01,79),LENGTH=0001,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'AGR-USER-ID'
ZFLD007  DFHMDF POS=(02,01),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(02,10),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CURR-RSPNS-ID-SY00'
ZFLD008  DFHMDF POS=(02,21),LENGTH=0008,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,30),LENGTH=0001,ATTRB=(PROT,ASKIP,BRT),        X
               INITIAL=':',COLOR=DEFAULT,HILIGHT=OFF
* 'AGR-FUNC-DESCRIPTION'
ZFLD009  DFHMDF POS=(02,32),LENGTH=0028,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,61),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'PAGE-STATUS-CODE-SY00'
ZFLD010  DFHMDF POS=(02,64),LENGTH=0010,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,75),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(06,11),LENGTH=0007,ATTRB=(PROT,ASKIP),            X
               INITIAL='VENDOR:',COLOR=BLUE,HILIGHT=OFF
* 'VNDR-ID-LAST-NINE-EX80'
ZFLD011  DFHMDF POS=(06,19),LENGTH=0009,ATTRB=(UNPROT,IC),             X
               COLOR=DEFAULT,HILIGHT=UNDERLINE
         DFHMDF POS=(06,29),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'VNDR-NAME-EX80'
ZFLD012  DFHMDF POS=(06,30),LENGTH=0035,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(06,66),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(08,01),LENGTH=0040,ATTRB=(PROT,ASKIP),            X
               INITIAL='ACT  STATEMENT     RUN DATE   CHECK NMBR',     X
               COLOR=BLUE,HILIGHT=OFF
         DFHMDF POS=(09,01),LENGTH=0040,ATTRB=(PROT,ASKIP),            X
               INITIAL='---  ---------     --------   ----------',     X
               COLOR=BLUE,HILIGHT=OFF
* 'ACTN-CODE-EX80'
ZFLD013  DFHMDF POS=(10,02),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(10,04),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCMNT-NMBR-EX80'
ZFLD014  DFHMDF POS=(10,07),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(10,16),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'STMNT-RUN-DATE-EX80'
ZFLD015  DFHMDF POS=(10,20),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(10,29),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CHECK-NMBR-EX80'
ZFLD016  DFHMDF POS=(10,32),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(10,41),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-EX80'
ZFLD017  DFHMDF POS=(11,02),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(11,04),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCMNT-NMBR-EX80'
ZFLD018  DFHMDF POS=(11,07),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(11,16),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'STMNT-RUN-DATE-EX80'
ZFLD019  DFHMDF POS=(11,20),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(11,29),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CHECK-NMBR-EX80'
ZFLD020  DFHMDF POS=(11,32),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(11,41),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-EX80'
ZFLD021  DFHMDF POS=(12,02),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(12,04),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCMNT-NMBR-EX80'
ZFLD022  DFHMDF POS=(12,07),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(12,16),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'STMNT-RUN-DATE-EX80'
ZFLD023  DFHMDF POS=(12,20),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(12,29),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CHECK-NMBR-EX80'
ZFLD024  DFHMDF POS=(12,32),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(12,41),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-EX80'
ZFLD025  DFHMDF POS=(13,02),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(13,04),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCMNT-NMBR-EX80'
ZFLD026  DFHMDF POS=(13,07),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(13,16),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'STMNT-RUN-DATE-EX80'
ZFLD027  DFHMDF POS=(13,20),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(13,29),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CHECK-NMBR-EX80'
ZFLD028  DFHMDF POS=(13,32),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(13,41),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-EX80'
ZFLD029  DFHMDF POS=(14,02),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(14,04),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCMNT-NMBR-EX80'
ZFLD030  DFHMDF POS=(14,07),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(14,16),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'STMNT-RUN-DATE-EX80'
ZFLD031  DFHMDF POS=(14,20),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(14,29),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CHECK-NMBR-EX80'
ZFLD032  DFHMDF POS=(14,32),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(14,41),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-EX80'
ZFLD033  DFHMDF POS=(15,02),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(15,04),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCMNT-NMBR-EX80'
ZFLD034  DFHMDF POS=(15,07),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(15,16),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'STMNT-RUN-DATE-EX80'
ZFLD035  DFHMDF POS=(15,20),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(15,29),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CHECK-NMBR-EX80'
ZFLD036  DFHMDF POS=(15,32),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(15,41),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-EX80'
ZFLD037  DFHMDF POS=(16,02),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(16,04),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCMNT-NMBR-EX80'
ZFLD038  DFHMDF POS=(16,07),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(16,16),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'STMNT-RUN-DATE-EX80'
ZFLD039  DFHMDF POS=(16,20),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(16,29),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CHECK-NMBR-EX80'
ZFLD040  DFHMDF POS=(16,32),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(16,41),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-EX80'
ZFLD041  DFHMDF POS=(17,02),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(17,04),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCMNT-NMBR-EX80'
ZFLD042  DFHMDF POS=(17,07),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(17,16),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'STMNT-RUN-DATE-EX80'
ZFLD043  DFHMDF POS=(17,20),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(17,29),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CHECK-NMBR-EX80'
ZFLD044  DFHMDF POS=(17,32),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(17,41),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-EX80'
ZFLD045  DFHMDF POS=(18,02),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(18,04),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCMNT-NMBR-EX80'
ZFLD046  DFHMDF POS=(18,07),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(18,16),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'STMNT-RUN-DATE-EX80'
ZFLD047  DFHMDF POS=(18,20),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(18,29),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CHECK-NMBR-EX80'
ZFLD048  DFHMDF POS=(18,32),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(18,41),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-EX80'
ZFLD049  DFHMDF POS=(19,02),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(19,04),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCMNT-NMBR-EX80'
ZFLD050  DFHMDF POS=(19,07),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(19,16),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'STMNT-RUN-DATE-EX80'
ZFLD051  DFHMDF POS=(19,20),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(19,29),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CHECK-NMBR-EX80'
ZFLD052  DFHMDF POS=(19,32),LENGTH=0008,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(19,41),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(21,01),LENGTH=0004,ATTRB=(PROT,ASKIP),            X
               INITIAL='REQ:',COLOR=BLUE,HILIGHT=OFF
* 'REQ-DCMNT-NMBR-EX80'
ZFLD053  DFHMDF POS=(21,07),LENGTH=0008,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(21,16),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCLINK-MESSAGE'
ZFLD054  DFHMDF POS=(21,80),LENGTH=0160,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(24,01),LENGTH=0012,ATTRB=(PROT,ASKIP,NUM),        X
               INITIAL='NEXT SCREEN:',COLOR=BLUE,HILIGHT=OFF
* 'AGR-PASSED-ONE'
ZFLD055  DFHMDF POS=(24,14),LENGTH=0030,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(24,45),LENGTH=0009,ATTRB=(PROT,ASKIP),            X
               INITIAL='RESPONSE:',COLOR=BLUE,HILIGHT=OFF
* 'AGR-MAP-RESPONSE'
ZFLD056  DFHMDF POS=(24,55),LENGTH=0008,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(24,64),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ERROR-TEXT-SY00'
ZFLD057  DFHMDF POS=(24,65),LENGTH=0014,ATTRB=(PROT,DRK),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMSD TYPE=FINAL
         END
