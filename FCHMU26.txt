FCHMU26  DFHMSD TYPE=MAP,LANG=COBOL,TERM=3270,MODE=INOUT,              X
               TIOAPFX=YES,MAPATTS=(COLOR,HILIGHT),                    X
               DSATTS=(COLOR,HILIGHT)
FCHMU26  DFHMDI SIZE=(24,80),LINE=1,COLUMN=1,CTRL=FREEKB,TIOAPFX=YES
         DFHMDF POS=(01,01),LENGTH=0007,ATTRB=(PROT,ASKIP),            X
               INITIAL='FCHMU26',COLOR=BLUE,HILIGHT=OFF
* 'TRMNL-ID-SY00'
ZFLD002  DFHMDF POS=(01,10),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(01,19),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'FULL-NAME-6001-SY00'
ZFLD003  DFHMDF POS=(01,23),LENGTH=0035,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(01,59),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'AGR-DATE'
ZFLD004  DFHMDF POS=(01,64),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'WORK-HH-MM-TIME-SY00'
ZFLD005  DFHMDF POS=(01,73),LENGTH=0005,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'AM-PM-FLAG-SY00'
ZFLD006  DFHMDF POS=(01,79),LENGTH=0001,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'AGR-USER-ID'
ZFLD007  DFHMDF POS=(02,01),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(02,10),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CURR-RSPNS-ID-SY00'
ZFLD008  DFHMDF POS=(02,21),LENGTH=0008,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,30),LENGTH=0001,ATTRB=(PROT,ASKIP,BRT),        X
               INITIAL=':',COLOR=DEFAULT,HILIGHT=OFF
* 'AGR-FUNC-DESCRIPTION'
ZFLD009  DFHMDF POS=(02,32),LENGTH=0028,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,61),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'PAGE-STATUS-CODE-SY00'
ZFLD010  DFHMDF POS=(02,64),LENGTH=0010,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,75),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(04,10),LENGTH=0010,ATTRB=(PROT,ASKIP),            X
               INITIAL='ACT (ACD):',COLOR=BLUE,HILIGHT=OFF
* 'ACTN-CODE-CH26'
ZFLD011  DFHMDF POS=(04,21),LENGTH=0001,ATTRB=(UNPROT,IC),             X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(04,23),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(05,16),LENGTH=0004,ATTRB=(PROT,ASKIP),            X
               INITIAL='COA:',COLOR=BLUE,HILIGHT=OFF
* 'COA-CODE-4008-CH26'
ZFLD012  DFHMDF POS=(05,21),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(05,23),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(05,29),LENGTH=0010,ATTRB=(PROT,ASKIP),            X
               INITIAL='FUND TYPE:',COLOR=BLUE,HILIGHT=OFF
* 'FUND-TYPE-CODE-4008-CH26'
ZFLD013  DFHMDF POS=(05,40),LENGTH=0002,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(05,43),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'FUND-TYPE-DESC-4009-CH26'
ZFLD014  DFHMDF POS=(05,44),LENGTH=0035,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(05,80),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(06,09),LENGTH=0011,ATTRB=(PROT,ASKIP),            X
               INITIAL='EFCTV DATE:',COLOR=BLUE,HILIGHT=OFF
* 'START-DATE-4009-CH26'
ZFLD015  DFHMDF POS=(06,21),LENGTH=0008,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(06,30),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(06,33),LENGTH=0017,ATTRB=(PROT,ASKIP),            X
               INITIAL='NEXT CHANGE DATE:',COLOR=BLUE,HILIGHT=OFF
* 'NEXT-CHNG-DATE-CH26'
ZFLD016  DFHMDF POS=(06,51),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(06,60),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(06,61),LENGTH=0007,ATTRB=(PROT,ASKIP),            X
               INITIAL='STATUS:',COLOR=BLUE,HILIGHT=OFF
* 'STATUS-DESC-CH26'
ZFLD017  DFHMDF POS=(06,69),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(06,78),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(07,10),LENGTH=0010,ATTRB=(PROT,ASKIP),            X
               INITIAL='TERM DATE:',COLOR=BLUE,HILIGHT=OFF
* 'END-DATE-4009-CH26'
ZFLD018  DFHMDF POS=(07,21),LENGTH=0008,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(07,30),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(07,31),LENGTH=0019,ATTRB=(PROT,ASKIP),            X
               INITIAL='LAST ACTIVITY DATE:',COLOR=BLUE,HILIGHT=OFF
* 'ACTVY-DATE-4008-CH26'
ZFLD019  DFHMDF POS=(07,51),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(07,60),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(07,61),LENGTH=0011,ATTRB=(PROT,ASKIP),            X
               INITIAL='EFCTV TIME:',COLOR=BLUE,HILIGHT=OFF
* 'TIME-STAMP-4009-CH26'
ZFLD020  DFHMDF POS=(07,73),LENGTH=0005,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
* 'AM-PM-FLAG-CH26'
ZFLD021  DFHMDF POS=(07,79),LENGTH=0001,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(08,01),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(08,05),LENGTH=0019,ATTRB=(PROT,ASKIP),            X
               INITIAL='INTERNAL FUND TYPE:',COLOR=BLUE,HILIGHT=OFF
* 'INTRL-FUND-TYPE-4009-CH26'
ZFLD022  DFHMDF POS=(08,25),LENGTH=0002,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(08,28),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'INTRL-FNDT-DESC-4012-CH26'
ZFLD023  DFHMDF POS=(08,34),LENGTH=0035,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(08,70),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(09,02),LENGTH=0022,ATTRB=(PROT,ASKIP),            X
               INITIAL='PREDECESSOR FUND TYPE:',COLOR=BLUE,            X
               HILIGHT=OFF
* 'PREDCSR-FUND-TYPE-4009-CH26'
ZFLD024  DFHMDF POS=(09,25),LENGTH=0002,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(09,28),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'PREDCSR-FNDT-DESC-4009-CH26'
ZFLD025  DFHMDF POS=(09,34),LENGTH=0035,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(09,70),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(10,04),LENGTH=0020,ATTRB=(PROT,ASKIP),            X
               INITIAL='CAPITALIZATION FUND:',COLOR=BLUE,HILIGHT=OFF
* 'CPTLZN-FUND-CODE-4009-CH26'
ZFLD026  DFHMDF POS=(10,25),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(10,32),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CPTLZN-FUND-TITLE-4005-CH26'
ZFLD027  DFHMDF POS=(10,34),LENGTH=0035,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(10,70),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(11,01),LENGTH=0023,ATTRB=(PROT,ASKIP),            X
               INITIAL='CAPITALIZATION ACCOUNT:',COLOR=BLUE,           X
               HILIGHT=OFF
* 'CPTLZN-ACCT-CODE-4009-CH26'
ZFLD028  DFHMDF POS=(11,25),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(11,32),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CPTLZN-ACCT-TITLE-4034-CH26'
ZFLD029  DFHMDF POS=(11,34),LENGTH=0035,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(11,70),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(12,01),LENGTH=0023,ATTRB=(PROT,ASKIP),            X
               INITIAL='DEFAULT OVERRIDE (O/F):',COLOR=BLUE,           X
               HILIGHT=OFF
* 'DFLT-FROM-IND-4009-CH26'
ZFLD030  DFHMDF POS=(12,25),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(12,27),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(13,38),LENGTH=0014,ATTRB=(PROT,ASKIP),            X
               INITIAL='BUDGET CONTROL',COLOR=BLUE,HILIGHT=OFF
         DFHMDF POS=(14,10),LENGTH=0014,ATTRB=(PROT,ASKIP),            X
               INITIAL='ACCOUNT INDEX:',COLOR=BLUE,HILIGHT=OFF
* 'ACCT-INDX-BDGT-CNTRL-4009-CH26'
ZFLD031  DFHMDF POS=(14,25),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(14,27),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACCT-INDX-BDGT-DESC-CH26'
ZFLD032  DFHMDF POS=(14,29),LENGTH=0035,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(14,65),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(15,19),LENGTH=0005,ATTRB=(PROT,ASKIP),            X
               INITIAL='FUND:',COLOR=BLUE,HILIGHT=OFF
* 'FUND-BDGT-CNTRL-4009-CH26'
ZFLD033  DFHMDF POS=(15,25),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(15,27),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'FUND-BDGT-DESC-CH26'
ZFLD034  DFHMDF POS=(15,29),LENGTH=0035,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(15,65),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(16,11),LENGTH=0013,ATTRB=(PROT,ASKIP),            X
               INITIAL='ORGANIZATION:',COLOR=BLUE,HILIGHT=OFF
* 'ORGZN-BDGT-CNTRL-4009-CH26'
ZFLD035  DFHMDF POS=(16,25),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(16,27),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ORGZN-BDGT-DESC-CH26'
ZFLD036  DFHMDF POS=(16,29),LENGTH=0035,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(16,65),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(17,16),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               INITIAL='ACCOUNT:',COLOR=BLUE,HILIGHT=OFF
* 'ACCT-BDGT-CNTRL-4009-CH26'
ZFLD037  DFHMDF POS=(17,25),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(17,27),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACCT-BDGT-DESC-CH26'
ZFLD038  DFHMDF POS=(17,29),LENGTH=0035,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(17,65),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(18,16),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               INITIAL='PROGRAM:',COLOR=BLUE,HILIGHT=OFF
* 'PRGRM-BDGT-CNTRL-4009-CH26'
ZFLD039  DFHMDF POS=(18,25),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(18,27),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'PRGRM-BDGT-DESC-CH26'
ZFLD040  DFHMDF POS=(18,29),LENGTH=0035,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(18,65),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(19,09),LENGTH=0015,ATTRB=(PROT,ASKIP),            X
               INITIAL='CONTROL PERIOD:',COLOR=BLUE,HILIGHT=OFF
* 'CNTRL-PRD-CODE-4009-CH26'
ZFLD041  DFHMDF POS=(19,25),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(19,27),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CNTRL-PRD-DESC-4012-CH26'
ZFLD042  DFHMDF POS=(19,29),LENGTH=0035,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(19,65),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(20,15),LENGTH=0009,ATTRB=(PROT,ASKIP),            X
               INITIAL='SEVERITY:',COLOR=BLUE,HILIGHT=OFF
* 'CNTRL-SVRTY-CODE-4009-CH26'
ZFLD043  DFHMDF POS=(20,25),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(20,27),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CNTRL-SVRTY-DESC-4012-CH26'
ZFLD044  DFHMDF POS=(20,29),LENGTH=0035,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(20,65),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCLINK-MESSAGE'
ZFLD045  DFHMDF POS=(21,80),LENGTH=0160,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(24,01),LENGTH=0012,ATTRB=(PROT,ASKIP,NUM),        X
               INITIAL='NEXT SCREEN:',COLOR=BLUE,HILIGHT=OFF
* 'AGR-PASSED-ONE'
ZFLD046  DFHMDF POS=(24,14),LENGTH=0030,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(24,45),LENGTH=0009,ATTRB=(PROT,ASKIP),            X
               INITIAL='RESPONSE:',COLOR=BLUE,HILIGHT=OFF
* 'AGR-MAP-RESPONSE'
ZFLD047  DFHMDF POS=(24,55),LENGTH=0008,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(24,64),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ERROR-TEXT-SY00'
ZFLD048  DFHMDF POS=(24,65),LENGTH=0014,ATTRB=(PROT,DRK),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMSD TYPE=FINAL
         END
