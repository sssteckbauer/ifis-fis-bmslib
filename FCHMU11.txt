FCHMU11  DFHMSD TYPE=MAP,LANG=COBOL,TERM=3270,MODE=INOUT,              X
               TIOAPFX=YES,MAPATTS=(COLOR,HILIGHT),                    X
               DSATTS=(COLOR,HILIGHT)
FCHMU11  DFHMDI SIZE=(24,80),LINE=1,COLUMN=1,CTRL=FREEKB,TIOAPFX=YES
         DFHMDF POS=(01,01),LENGTH=0007,ATTRB=(PROT,ASKIP),            X
               INITIAL='FCHMU11',COLOR=BLUE,HILIGHT=OFF
* 'TRMNL-ID-SY00'
ZFLD002  DFHMDF POS=(01,10),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(01,19),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'FULL-NAME-6001-SY00'
ZFLD003  DFHMDF POS=(01,23),LENGTH=0035,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(01,59),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'AGR-DATE'
ZFLD004  DFHMDF POS=(01,64),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'WORK-HH-MM-TIME-SY00'
ZFLD005  DFHMDF POS=(01,73),LENGTH=0005,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'AM-PM-FLAG-SY00'
ZFLD006  DFHMDF POS=(01,79),LENGTH=0001,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'AGR-USER-ID'
ZFLD007  DFHMDF POS=(02,01),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(02,10),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CURR-RSPNS-ID-SY00'
ZFLD008  DFHMDF POS=(02,21),LENGTH=0008,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,30),LENGTH=0001,ATTRB=(PROT,ASKIP,BRT),        X
               INITIAL=':',COLOR=DEFAULT,HILIGHT=OFF
* 'AGR-FUNC-DESCRIPTION'
ZFLD009  DFHMDF POS=(02,32),LENGTH=0028,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,61),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'PAGE-STATUS-CODE-SY00'
ZFLD010  DFHMDF POS=(02,64),LENGTH=0010,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,75),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(04,06),LENGTH=0010,ATTRB=(PROT,ASKIP),            X
               INITIAL='ACT (ACD):',COLOR=BLUE,HILIGHT=OFF
* 'ACTN-CODE-CH11'
ZFLD011  DFHMDF POS=(04,17),LENGTH=0001,ATTRB=(UNPROT,IC),             X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(04,19),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(06,12),LENGTH=0004,ATTRB=(PROT,ASKIP),            X
               INITIAL='COA:',COLOR=BLUE,HILIGHT=OFF
* 'COA-CODE-4050-CH11'
ZFLD012  DFHMDF POS=(06,17),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(06,19),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(06,23),LENGTH=0009,ATTRB=(PROT,ASKIP),            X
               INITIAL='ACTIVITY:',COLOR=BLUE,HILIGHT=OFF
* 'ACTVY-CODE-4050-CH11'
ZFLD013  DFHMDF POS=(06,33),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(06,40),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTVY-TITLE-4066-CH11'
ZFLD014  DFHMDF POS=(06,44),LENGTH=0035,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(06,80),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(08,05),LENGTH=0011,ATTRB=(PROT,ASKIP),            X
               INITIAL='EFCTV DATE:',COLOR=BLUE,HILIGHT=OFF
* 'START-DATE-4066-CH11'
ZFLD015  DFHMDF POS=(08,17),LENGTH=0008,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(08,26),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(08,30),LENGTH=0017,ATTRB=(PROT,ASKIP),            X
               INITIAL='NEXT CHANGE DATE:',COLOR=BLUE,HILIGHT=OFF
* 'NEXT-CHNG-DATE-CH11'
ZFLD016  DFHMDF POS=(08,48),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(08,57),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(08,63),LENGTH=0007,ATTRB=(PROT,ASKIP),            X
               INITIAL='STATUS:',COLOR=BLUE,HILIGHT=OFF
* 'STATUS-DESC-CH11'
ZFLD017  DFHMDF POS=(08,71),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(08,80),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(10,06),LENGTH=0010,ATTRB=(PROT,ASKIP),            X
               INITIAL='TERM DATE:',COLOR=BLUE,HILIGHT=OFF
* 'END-DATE-4066-CH11'
ZFLD018  DFHMDF POS=(10,17),LENGTH=0008,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(10,26),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(10,28),LENGTH=0019,ATTRB=(PROT,ASKIP),            X
               INITIAL='LAST ACTIVITY DATE:',COLOR=BLUE,HILIGHT=OFF
* 'ACTVY-DATE-4050-CH11'
ZFLD019  DFHMDF POS=(10,48),LENGTH=0008,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(10,57),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(10,59),LENGTH=0011,ATTRB=(PROT,ASKIP),            X
               INITIAL='EFCTV TIME:',COLOR=BLUE,HILIGHT=OFF
* 'TIME-STAMP-4066-CH11'
ZFLD020  DFHMDF POS=(10,71),LENGTH=0005,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
* 'AM-PM-FLAG-CH11'
ZFLD021  DFHMDF POS=(10,77),LENGTH=0001,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(10,79),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCLINK-MESSAGE'
ZFLD022  DFHMDF POS=(21,80),LENGTH=0160,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(24,01),LENGTH=0012,ATTRB=(PROT,ASKIP,NUM),        X
               INITIAL='NEXT SCREEN:',COLOR=BLUE,HILIGHT=OFF
* 'AGR-PASSED-ONE'
ZFLD023  DFHMDF POS=(24,14),LENGTH=0030,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(24,45),LENGTH=0009,ATTRB=(PROT,ASKIP),            X
               INITIAL='RESPONSE:',COLOR=BLUE,HILIGHT=OFF
* 'AGR-MAP-RESPONSE'
ZFLD024  DFHMDF POS=(24,55),LENGTH=0008,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(24,64),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ERROR-TEXT-SY00'
ZFLD025  DFHMDF POS=(24,65),LENGTH=0014,ATTRB=(PROT,DRK),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMSD TYPE=FINAL
         END
